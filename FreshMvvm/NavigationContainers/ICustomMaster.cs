﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FreshMvvm.NavigationContainers
{
    public interface ICustomMaster
    {
        /// <summary>
        /// This property will be replaced by CustomMasterDetailNavigation
        /// </summary>
        StackLayout ElemsStack { get; set; }
    }
}
