﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FreshMvvm.Annotations;
using FreshMvvm.Base;
using FreshMvvm.Extensions;
using FreshMvvm.IoC;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace FreshMvvm.NavigationContainers
{
    public class CustomMasterDetailNavigationContainer : MasterDetailPage, IFreshNavigationService
    {
        public Color ColorForSelectedItem { get; set; } = Color.Aquamarine;
        List<Page> _pagesInner = new List<Page>();
        ContentPage _menuPage;
        private StackLayout _masterMenuList;
        private Color _selectedItemOldColor;
        private ContentView _selectedItem;

        public ContentView SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != null)
                    _selectedItem.BackgroundColor = _selectedItemOldColor;
                _selectedItemOldColor = value.BackgroundColor;
                value.BackgroundColor = ColorForSelectedItem;
                _selectedItem = value;
            }
        }

        public Dictionary<ContentView, Page> DetailComponents { get; } = new Dictionary<ContentView, Page>();

        protected ObservableCollection<ContentView> MasterComponents { get; } = new ObservableCollection<ContentView>();

        public CustomMasterDetailNavigationContainer() : this(Constants.DefaultNavigationServiceName)
        {
        }

        public CustomMasterDetailNavigationContainer(string navigationServiceName)
        {
            NavigationServiceName = navigationServiceName;
            RegisterNavigation();
        }

        public void Init(string menuTitle, string menuIcon = null)
        {
            CreateMenuPage(menuTitle, menuIcon);
            RegisterNavigation();
        }
        public void Init<TMaster, TViewModel>(string menuTitle, string menuIcon = null) where TMaster : ContentPage, ICustomMaster
            where TViewModel : FreshPageModel
        {
            CreateMenuPage<TMaster,TViewModel>(menuTitle, menuIcon);
            RegisterNavigation();
        }

        

        protected virtual void RegisterNavigation()
        {
            FreshIoC.Container.Register<IFreshNavigationService>(this, NavigationServiceName);
        }

        public virtual TDetailViewModel AddPage<TDetailViewModel>(object masterData = null, object detailData = null)
            where TDetailViewModel : FreshPageModel
        {
            var detailPage = FreshPageModelResolver.ResolvePageModel<TDetailViewModel>(detailData);
            var commonViewModel = detailPage.GetModel();
            commonViewModel.CurrentNavigationServiceName = NavigationServiceName;
            _pagesInner.Add(detailPage);


            //get master elem and add it to menu
            var masterElem = FreshPageModelResolver.ResolveElemView(commonViewModel);
            TapGestureRecognizer tapRecognizer = new TapGestureRecognizer();
            tapRecognizer.Tapped += MenuItem_Clicked;
            masterElem.GestureRecognizers.Add(tapRecognizer);
            Device.BeginInvokeOnMainThread(() => { _masterMenuList.Children.Add(masterElem); });


            var navigationContainer = CreateContainerPage(detailPage);
            DetailComponents.Add(masterElem, navigationContainer);

            MasterComponents.Add(masterElem);


            if (DetailComponents.Count == 1)
                Detail = navigationContainer;
            return commonViewModel as TDetailViewModel;
        }

        internal Page CreateContainerPageSafe(Page page)
        {
            if (page is NavigationPage || page is MasterDetailPage || page is TabbedPage)
                return page;

            return CreateContainerPage(page);
        }

        protected virtual Page CreateContainerPage(Page page)
        {
            return new NavigationPage(page);
        }


        public void MenuItem_Clicked(object sender, EventArgs args)
        {
            if (DetailComponents.ContainsKey((ContentView) sender))
            {
                Detail = DetailComponents[(ContentView) sender];
                SelectedItem = (ContentView) sender;
            }

            IsPresented = false;
        }
        protected virtual void CreateMenuPage<TMaster, TViewModel>(string menuTitle, string menuIcon) where TMaster : ContentPage, ICustomMaster
            where TViewModel : FreshPageModel
        {
            FreshPageModelMapper.SetMappingRuleForFullView<TViewModel, TMaster>();
            var page = FreshPageModelResolver.ResolvePageModel<TViewModel>() as TMaster;
            _masterMenuList = page.ElemsStack;
            _menuPage = page;
            FinishCreating(menuTitle, menuTitle);
        }
        protected virtual void CreateMenuPage(string menuPageTitle, string menuIcon = null)
        {
            _masterMenuList = new StackLayout();
            _menuPage = new ContentPage();
            _menuPage.Content = new ScrollView() { Content = _masterMenuList };
            FinishCreating(menuIcon, menuPageTitle);
            
        }

        private void FinishCreating(string menuIcon, string menuPageTitle)
        {

            _menuPage.Title = menuPageTitle;
            _masterMenuList.Orientation = StackOrientation.Vertical;
            var navPage = new NavigationPage(_menuPage)
            {
                Title = "Menu"
            };
            
            if (!string.IsNullOrEmpty(menuIcon))
                navPage.Icon = menuIcon;
            Master = navPage;
        }
        public Task PushPage(Page page, FreshPageModel model, bool modal = false, bool animate = true)
        {
            return modal
                ? Navigation.PushModalAsync(CreateContainerPageSafe(page))
                : (Detail as NavigationPage)?.PushAsync(page, animate);
        }

        public Task PopPage(bool modal = false, bool animate = true)
        {
            return modal
                ? Navigation.PopModalAsync(animate)
                : (Detail as NavigationPage)?.PopAsync(animate);
        }

        public Task PopToRoot(bool animate = true)
        {
            return (Detail as NavigationPage)?.PopToRootAsync(animate);
        }

        public string NavigationServiceName { get; }

        public void NotifyChildrenPageWasPopped()
        {
            var master = Master as NavigationPage;
            master?.NotifyAllChildrenPopped();

            foreach (var page in DetailComponents.Values)
            {
                var navigationPage = page as NavigationPage;
                navigationPage?.NotifyAllChildrenPopped();
            }
        }

        public Task<FreshPageModel> SwitchSelectedRootPageModel<T>() where T : FreshPageModel
        {
            var tabIndex = _pagesInner.FindIndex(o => o.GetModel().GetType().FullName == typeof(T).FullName);
            SelectedItem = MasterComponents[tabIndex];
            return Task.FromResult((Detail as NavigationPage)?.CurrentPage.GetModel());
        }
    }
}