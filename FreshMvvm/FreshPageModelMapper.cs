﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FreshMvvm.Base;
using Xamarin.Forms;

namespace FreshMvvm
{
    public class FreshPageModelMapper : IFreshPageModelMapper
    {
        private static IList<Type> _viewTypes;
        private static readonly ConcurrentDictionary<Type, Type> CustomMappingRulesForElems = new ConcurrentDictionary<Type, Type>();
        private static readonly ConcurrentDictionary<Type, Type> CustomMappingRulesForFullViews = new ConcurrentDictionary<Type, Type>();

        public static void SetMappingRuleForElem<TViewModel, TElemView>() where TViewModel : FreshPageModel where TElemView : ContentView
        {
            CustomMappingRulesForElems.AddOrUpdate(typeof(TViewModel),typeof(TElemView), (key, existingVal) => typeof(TElemView));
        }
        public static void SetMappingRuleForFullView<TViewModel, TView>() where TViewModel : FreshPageModel where TView : ContentPage
        {
            CustomMappingRulesForFullViews.AddOrUpdate(typeof(TViewModel), typeof(TView), (key, existingVal) => typeof(TView));
        }

        public string GetPageTypeName(Type pageModelType)
        {
            Type result;
            if (CustomMappingRulesForFullViews.TryGetValue(pageModelType, out result))
                return result.AssemblyQualifiedName;
            var first = GetCandidates(pageModelType).FirstOrDefault(t => !t.Name.Contains("ElemView") && t.Name.Contains("View"));
            if (first != null)
                return first.AssemblyQualifiedName;
            return $"TYPENOTFOUND for {pageModelType.AssemblyQualifiedName}";
        }
        public string GetContentViewTypeName(Type pageModelType)
        {
            Type result;
            if (CustomMappingRulesForElems.TryGetValue(pageModelType, out result))
                return result.AssemblyQualifiedName;
            var first = GetCandidates(pageModelType).FirstOrDefault(t => t.Name.Contains("ElemView"));
            if (first != null)
                return first.AssemblyQualifiedName;
            var defaultElemView = _viewTypes.FirstOrDefault(t => t.Name.Equals("DefaultElemView"));
            if (defaultElemView != null)
                return defaultElemView.AssemblyQualifiedName;
            return $"TYPENOTFOUND for {pageModelType.AssemblyQualifiedName}";
            /*return pageModelType.AssemblyQualifiedName
                .Replace("PageModel", "ElemView")
                .Replace("ViewModel", "ElemView")
                .Replace(".ElemViews.",".Views.");*/
        }

        public IEnumerable<Type> GetCandidates(Type pageModelType)
        {
            if (_viewTypes == null)
            {
                Assembly assembly = pageModelType.GetTypeInfo().Assembly;
                _viewTypes = assembly.GetTypes().Where(t => t.AssemblyQualifiedName.Contains(".Views.")).ToList();
            }
            var prefix = pageModelType.Name.Replace("ViewModel", "");
            return _viewTypes.Where(t => t.Name.Contains(prefix));
        }
    }
}

