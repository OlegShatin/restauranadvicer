﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantAdviser.Common
{
    public interface ILogger
    {
        void Log(string log);
    }
}
