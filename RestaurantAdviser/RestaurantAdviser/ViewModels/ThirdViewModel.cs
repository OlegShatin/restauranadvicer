﻿using System;
using System.Collections.Generic;
using System.Text;
using FreshMvvm.Base;
using PropertyChanged;
using RestaurantAdviser.Services.Interfaces;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ThirdViewModel : FreshPageModel
    {
        //prop for using in static contexts
        public static string Title { get; } = "Title from ThirdViewModel";
        //prop for using in binding
        public string TitleVal => Title;
       
    }
}