﻿using System;
using System.Collections.Generic;
using System.Text;
using FreshMvvm.Base;
using PropertyChanged;
using RestaurantAdviser.Services.Interfaces;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class SimpleItemViewModel : FreshPageModel
    {
        //prop for using in static contexts
        public static string Title { get; } = "Property from SimpleItemViewModel";
        //prop for using in binding
        public string TitleVal => Title;

        private readonly ISampleService _service;

        public SimpleItemViewModel(ISampleService service)
        {
            _service = service;
        }

        //do not use new command for each call - save command into private field
        private Command _getValueFromServiceCommand;

        public Command GetValueFromServiceCommand => 
            _getValueFromServiceCommand ?? (_getValueFromServiceCommand = new Command(async() =>
            {
                //use async methods to not block UI thread
                LastServiceResult =  await _service.TrueOrFalseOracleAsync();
            }));

        public bool LastServiceResult { get; set; }

        /*
         * Sample - how to checkout betweeen Navigation stacks
        private Command _toAnotherNavStackCommand;
        public Command ToAnotherNavStackCommand
        {
            get
            {
                if (_toAnotherNavStackCommand == null)
                    _toAnotherNavStackCommand = new Command(() =>
                    {
                        //method switches between several (2 in our sample) nav stacks
                        Navigation.SwitchOutRootNavigation(Data.Consts.NavigationContainerNames.NAME_OF_NAV_STACK_HERE);
                    });
                return _toAnotherNavStackCommand;
            }
        }*/
    }
}