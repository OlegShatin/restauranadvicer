﻿using System;
using System.Collections.Generic;
using System.Text;
using FreshMvvm.Base;
using PropertyChanged;
using RestaurantAdviser.Common;
using RestaurantAdviser.Services.Interfaces;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class CustomMasterViewModel : FreshPageModel
    {
        public CustomMasterViewModel(CustomMasterConsole console)
        {
            console.SetModel(this);
        }
        //prop for using in static contexts
        public static string Title { get; } = "Property from CustomMasterViewModel";
        //prop for using in binding
        public string TitleVal => Title;
        public string LogOutput { get; set; } = "";


    }

    public class CustomMasterConsole : ILogger
    {
        private CustomMasterViewModel _model;

        public void SetModel(CustomMasterViewModel model)
        {
            _model = model;
        }
        public void Log(string log)
        {
            if (_model != null)
                _model.LogOutput += log;
        }
    }
}