﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using FreshMvvm.Base;
using PropertyChanged;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class StartViewModel : FreshPageModel
    {
        //prop for using in static contexts
        public static string Title { get; } = "MasterDetailNavStack";
        //prop for using in binding
        public string TitleVal => Title;
        

        
        private Command _addCommand;
        /// <summary>
        /// Command to add VM to navigation
        /// VM with class name 'SampleViewModel' should have two Views - ( SampleElemView : ContentView) and (SampleView  : ContentPage)
        /// See 'Views' namespace
        /// </summary>
        public Command AddCommand
        {
            get
            {
                if (_addCommand == null)
                    _addCommand = new Command(() =>
                    {
                        Navigation.AddPageToNavigation<SimpleItemViewModel>();
                    });
                return _addCommand;
            }
        }
    }
}
