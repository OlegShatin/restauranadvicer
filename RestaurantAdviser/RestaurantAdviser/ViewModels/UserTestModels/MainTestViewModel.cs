﻿using System.Collections.ObjectModel;
using FreshMvvm.Base;
using PropertyChanged;
using RestaurantAdviser.DAL;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.Generated;
using RestaurantAdviser.Services.Interfaces;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels.UserTestModels
{
    [AddINotifyPropertyChangedInterface]
    public class MainTestViewModel : FreshPageModel
    {
        private readonly IUserTestService _testService;
        private readonly IRepository<Dish> _dishRepository;
        //prop for using in static contexts
        public static string Title { get; } = "Подбор меню";
        //prop for using in binding
        public string TitleVal => Title;
        public UserTestResult Test { get; set; }

        //private readonly IUserTestService _testService;

        public MainTestViewModel(IUserTestService testService, IRepository<Dish> dishRepository)
        {
            _testService = testService;
            _dishRepository = dishRepository;
            Test = _testService.GetStartTest();
        }

        //do not use new command for each call - save command into private field
        private Command _endTestCommand;
        public TestStartViewModel TestStart { get; private set; }
        public Command EndTestCommand => 
            _endTestCommand ?? (_endTestCommand = new Command(async() =>
            {
                TestStart.ResultOrders = new ObservableCollection<Order>(await _testService.OfferOrdersByTest(Test));
                await Navigation.PopPageModel();
            }));


        public void SetTestStart(TestStartViewModel testStartViewModel)
        {
            TestStart = testStartViewModel;
        }
    }
}