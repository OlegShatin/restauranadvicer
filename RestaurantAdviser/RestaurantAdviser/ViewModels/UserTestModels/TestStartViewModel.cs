﻿using System;
using System.Collections.ObjectModel;
using FreshMvvm.Base;
using PropertyChanged;
using RestaurantAdviser.Models.Generated;
using RestaurantAdviser.Services.Interfaces;
using Xamarin.Forms;

namespace RestaurantAdviser.ViewModels.UserTestModels
{
    [AddINotifyPropertyChangedInterface]
    public class TestStartViewModel : FreshPageModel
    {
        //prop for using in static contexts
        public static string Title { get; } = "Тест";
        //prop for using in binding
        public string TitleVal => Title;
        public int Page { get; set; } = 0;

        //private readonly IUserTestService _testService;

        public TestStartViewModel()
        {
            //_testService = testService;
        }

        //do not use new command for each call - save command into private field
        private Command _startTestCommand;

        public Command StartTestCommand => 
            _startTestCommand ?? (_startTestCommand = new Command(async() =>
            {
                try
                {
                    var runtest = await Navigation.PushPageModel<MainTestViewModel>();
                    runtest.SetTestStart(this);
                }
                catch (Exception e)
                {
                    
                }
                
            }));

        public ObservableCollection<Order> ResultOrders { get; set; }
    }
}