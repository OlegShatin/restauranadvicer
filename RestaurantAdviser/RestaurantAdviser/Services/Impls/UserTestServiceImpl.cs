﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantAdviser.DAL;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.Generated;
using RestaurantAdviser.Models.RequestModels;
using RestaurantAdviser.Services.Interfaces;

namespace RestaurantAdviser.Services.Impls
{
    public class UserTestServiceImpl : IUserTestService
    {
        private readonly IRepository<Dish> _dishesRepository;
        private readonly IRepository<Restaurant> _restaurantsRepository;
        private const int MaxTakeSize = 100;
        private const int OfferOrdersMaxCount = 4;

        public UserTestServiceImpl(IRepository<Dish> dishesRepository, IRepository<Restaurant> restaurantsRepository)
        {
            _dishesRepository = dishesRepository;
            _restaurantsRepository = restaurantsRepository;
        }

        public async Task<IEnumerable<Order>> OfferOrdersByTest(UserTestResult testResult)
        {
            IEnumerable<Dish> dishes = null;
            if (testResult.MainDishes == null || !testResult.MainDishes.Any())
            {
                dishes =
                    await _dishesRepository.Filter(new DishRequest()
                    {
                        SearchByNames = GetSearchNames(testResult.EatType),
                        RequiredType = DishesType.Eat,
                        Take = MaxTakeSize
                    });
                if (testResult.WithoutMeat)
                {
                    dishes = dishes.GetDishesWithoutMeat();
                }
            }

            IEnumerable<Dish> drinks;
            if (testResult.Drink == null && testResult.DrinkType != DrinkType.None &&
                testResult.DrinkType != DrinkType.Any)
            {
                drinks =
                    await _dishesRepository.Filter(new DishRequest()
                    {
                        SearchByNames = GetSearchNames(testResult.DrinkType),
                        RequiredType = DishesType.Drink,
                        Take = MaxTakeSize
                    });
            }


            var res = new List<Order>();
            var usedRestsIds = new List<int>();
            int dishPos = 0;
            var rests = await _restaurantsRepository.GetAll();
            bool cancelled = false;
            for (var i = 0; i < OfferOrdersMaxCount; i++)
            {
                Dish currDish = null;
                Restaurant candidateRest = null;
                var order = new Order();
                if (testResult.MainDishes == null || !testResult.MainDishes.Any())
                {
                    do
                    {
                        if (dishes.Count() <= dishPos)
                        {
                            cancelled = true;
                            break;
                        }
                        currDish = dishes.ElementAt(dishPos);
                        dishPos++;
                        candidateRest =
                            rests.FirstOrDefault(
                                r =>
                                    usedRestsIds.All(usedId => usedId != r.Id) &&
                                    r.RestaurantDishes.Any(rd => rd.DishId == currDish.Id));
                    } while (candidateRest == null);
                    if (cancelled)
                        break;
                    order.Restaurant = candidateRest;
                    usedRestsIds.Add(candidateRest.Id);
                    order.Dishes.Add(currDish);
                }
                else
                {
                    int j = 0;
                    do
                    {
                        if (j == rests.Count())
                        {
                            cancelled = true;
                            break;
                        }
                        candidateRest =
                            rests.FirstOrDefault(
                                r =>
                                    usedRestsIds.All(usedId => usedId != r.Id) &&
                                    testResult.MainDishes.All(
                                        selectedDish => r.RestaurantDishes.Any(rd => rd.DishId == selectedDish.Id)));
                        j++;
                    } while (candidateRest == null);
                    if (cancelled)
                        break;
                    order.Restaurant = candidateRest;
                    order.Dishes = testResult.MainDishes;
                }
                res.Add(order);
            }
            //todo: solve drinks
            return res;
        }

        public UserTestResult GetStartTest()
        {
            return new UserTestResult();
        }

        private IEnumerable<string> GetSearchNames(DrinkType type)
        {
            IEnumerable<string> result;
            switch (type)
            {
                case DrinkType.Beer:
                    return new List<string>() {"пиво", "стаут", "эль"};
                case DrinkType.Cofee:
                    return new List<string>() {"кофе", "капучино", "латте", "американо", "эспрессо"};
                case DrinkType.Juice:
                    return new List<string>() {"фреш", "сок", "свежевыжат"};
                case DrinkType.Soda:
                    return new List<string>()
                    {
                        "кола",
                        "cola",
                        "fanta",
                        "sprite",
                        "pepsi",
                        "mirinda",
                        "7up",
                        "mountain",
                        "лимонад"
                    };
                case DrinkType.Tea:
                    return new List<string>() {"чай", "улун"};
                case DrinkType.Wine:
                    return new List<string>() {"вино"};
                default:
                    return null;
            }
        }

        private IEnumerable<string> GetSearchNames(EatType type)
        {
            switch (type)
            {
                case EatType.Burger:
                    return new List<string>() {"бургер"};
                case EatType.Desert:
                    return new List<string>()
                    {
                        "десерт",
                        "мороженное",
                        "торт",
                        "пирог",
                        "мороженое",
                        "чизкейк",
                        "лакомство"
                    };
                case EatType.HomeDish:
                    return new List<string>() {"гуляш", "сковородк", "паста", "сосис", "домашн", "традиц"};
                case EatType.Pizza:
                    return new List<string>() {"пицца", "пеперонни"};
                case EatType.Salad:
                    return new List<string>() {"цезарь", "крабовый", "салат"};
                case EatType.Soup:
                    return new List<string>() {"борщ", "суп", "окрошка", "мисо"};
                default:
                    return null;
            }
        }
    }

    public static class DishesExtensions
    {
        public static IEnumerable<Dish> GetDishesWithoutMeat(this IEnumerable<Dish> source)
        {
            return
                source.Where(
                    d =>
                        !MeatNames.Any(
                            meat => d.Name.ToLower().Contains(meat) || d.Description.ToLower().Contains(meat)));
        }

        private static readonly IEnumerable<string> MeatNames
            = new List<string>()
            {
                "мясн",
                "мясо",
                "телятина",
                "свинина",
                "сосиск",
                "котлет",
                "курин",
                "куриц",
                "куроч",
                "окорочк",
                "купат",
                "говяд"
            };
    }
}