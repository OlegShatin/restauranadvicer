﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantAdviser.Services.Interfaces;

namespace RestaurantAdviser.Services.Impls
{
    public class SampleServiceImpl : ISampleService
    {
        //some impl for random true or false
        public Task<bool> TrueOrFalseOracleAsync()
        {
            return Task.Run(() =>
            {
                Task.Delay(5 * 1000).Wait();

                return DateTime.Now.Second % 2 == 0;
            });

        }
    }
}
