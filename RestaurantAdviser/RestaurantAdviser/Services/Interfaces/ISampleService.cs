﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantAdviser.Services.Interfaces
{
    public interface ISampleService
    {
        Task<bool> TrueOrFalseOracleAsync();
    }
}
