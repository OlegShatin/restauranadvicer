﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestaurantAdviser.Models.Generated;

namespace RestaurantAdviser.Services.Interfaces
{
    public interface IUserTestService
    {
        UserTestResult GetStartTest();
        Task<IEnumerable<Order>> OfferOrdersByTest(UserTestResult testResult);
    }
}
