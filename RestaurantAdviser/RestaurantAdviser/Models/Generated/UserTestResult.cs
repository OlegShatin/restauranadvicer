﻿using System;
using System.Collections.Generic;
using System.Text;
using PropertyChanged;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.Models.Generated
{
    public enum DrinkType
    {
        None, Cofee, Tea, Wine, Beer, Juice, Soda, Any
    }

    public enum EatType
    {
        Salad, Soup, Pizza, Burger, HomeDish, Desert
    }
    [AddINotifyPropertyChangedInterface]
    public class UserTestResult
    {
        public int MaxCost { get; set; } = 1000;
        public bool WithoutMeat { get; set; } = false;
        /// <summary>
        /// Drink != null - concrete drink should be in order. else choose by DrinkType
        /// </summary>
        public DrinkType DrinkType { get; set; } = DrinkType.None;
        public Dish Drink { get; set; }
        /// <summary>
        /// if Main dishes not specified, then selec dish by type
        /// </summary>
        public EatType EatType { get; set; }
        public IList<Dish> MainDishes { get; set; }
        public bool NearMeRequired { get; set; } = true;
    }
}
