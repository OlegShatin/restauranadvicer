﻿using System;
using System.Collections.Generic;
using System.Text;
using PropertyChanged;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.Models.Generated
{
    [AddINotifyPropertyChangedInterface]
    public class Order
    {
        public Restaurant Restaurant { get; set; }
        public IList<Dish> Dishes { get; set; } = new List<Dish>();
        public int Total { get; set; }
    }
}
