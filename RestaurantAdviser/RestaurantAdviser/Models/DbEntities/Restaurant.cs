﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PropertyChanged;

namespace RestaurantAdviser.Models.DbEntities
{
    [AddINotifyPropertyChangedInterface]
    public class Restaurant
    {
        public int Id { get; set; }
        [MaxLength(1000)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Address { get; set; }
        [MaxLength(1000)]
        public string Location { get; set; }
        [MaxLength(1500)]
        public string Image_url { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        [MaxLength(1000)]
        public string Short_description { get; set; }

        public IList<RestaurantDish> RestaurantDishes { get; set; }
    }
}
