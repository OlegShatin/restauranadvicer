﻿using System.ComponentModel.DataAnnotations;
using PropertyChanged;

namespace RestaurantAdviser.Models.DbEntities
{
    public enum DishesType
    {
        Eat = 0,
        Drink = 1
    }
    [AddINotifyPropertyChangedInterface]
    public class Dish
    {
        public int Id { get; set; }
        [MaxLength(500)]
        public string Name { get; set; }
        [MaxLength(1500)]
        public string Description { get; set; }
        [MaxLength(100000)]
        public decimal Cost { get; set; }
        [MaxLength(20)]
        public int Caloricity { get; set; }
        [MaxLength(1500)]
        public string Image_url { get; set; }
        public DishesType? Type { get; set; }
    }
}
