﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RestaurantAdviser.Models.DbEntities
{
    public class Ingredient
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
       
    }
}
