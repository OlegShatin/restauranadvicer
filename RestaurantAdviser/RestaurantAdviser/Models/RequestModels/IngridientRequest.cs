﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.Models.RequestModels
{
    public class IngredientRequest : IRequestModel<Ingredient>
    {
        public int? Take { get; set; }
        public int? Offset { get; set; }
        public IEnumerable<string> SearchByNames { get; set; }
    }
}
