﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantAdviser.DAL;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.Models.RequestModels
{
    public class DishRequest : IRequestModel<Dish>
    {
        public IEnumerable<int> ContainedIngredients { get; set; }
        public int? CaloricityLessThan { get; set; }
        public int? CaloricityGreatherThan { get; set; }
        public DishesType? RequiredType { get; set; }
        public int? Take { get; set; }
        public int? Offset { get; set; }
        public IEnumerable<string> SearchByNames { get; set; }
    }
}
