﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.Models.RequestModels
{
    public class RestaurantRequest : IRequestModel<Restaurant>
    {
        public IEnumerable<string> ChooseLocation { get; set; }
        public IEnumerable<int> ContainedDishes { get; set; }
        public int? Take { get; set; }
        public int? Offset { get; set; }
    }
}
