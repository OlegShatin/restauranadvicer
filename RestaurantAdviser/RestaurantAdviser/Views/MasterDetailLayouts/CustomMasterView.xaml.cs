﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm.NavigationContainers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestaurantAdviser.Views.MasterDetailLayouts
{
	
	public partial class CustomMasterView : ContentPage, ICustomMaster
	{
	    
		public CustomMasterView ()
		{

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
		}

	    public StackLayout ElemsStack { get { return StackToInsert; } set { StackToInsert = value; } }
	}
}