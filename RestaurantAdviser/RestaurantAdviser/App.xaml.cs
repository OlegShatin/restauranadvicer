﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FreshMvvm;
using FreshMvvm.NavigationContainers;
using RestaurantAdviser.Services.Impls;
using RestaurantAdviser.Services.Interfaces;
using RestaurantAdviser.ViewModels;
using Xamarin.Forms;
using FreshMvvm.IoC;
using RestaurantAdviser.Common;
using RestaurantAdviser.DAL;
using RestaurantAdviser.DAL.Repositories;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.ViewModels.UserTestModels;
using RestaurantAdviser.Views.MasterDetailLayouts;
using RestaurantAdviser.Views.MenuElements;

namespace RestaurantAdviser
{
    public partial class App : Application
    {
        public App()
        {
            
            //register in ioc-container
            //register all your services here. you can use AsMultiInstance and AsSingletone
            FreshIoC.Container.Register<ISampleService, SampleServiceImpl>().AsMultiInstance();
            FreshIoC.Container.Register<ILogger, CustomMasterConsole>().AsSingleton();
            FreshIoC.Container.Register<RestClient, RestClient>().AsSingleton();
            FreshIoC.Container.Register<IRepository<Dish>, DishWebRepository>().AsMultiInstance();
            FreshIoC.Container.Register<IRepository<Restaurant>, RestaurantWebRepository>().AsMultiInstance();
            FreshIoC.Container.Register<IUserTestService, UserTestServiceImpl>().AsMultiInstance();
            //abiblity to customize mapping for view models
            //if there is no such custom rule or no matching class by naming convention then will be used 
            //view with name 'DefaultElemView' (for Elems only).
            //in this case, Second and Third ViewModel's have no match ElemView,
            //but we want to share elem view for them
            FreshPageModelMapper.SetMappingRuleForElem<ThirdViewModel, SharedElemView>();
            FreshPageModelMapper.SetMappingRuleForElem<SecondViewModel, SharedElemView>();
            
 
            InitializeComponent();
            

            //create mater-detail as another
            var masterDetailNavContainer = new CustomMasterDetailNavigationContainer(Data.Consts.NavigationContainerNames.MainContainer);
            //masterDetailNavContainer.Init<CustomMasterView, CustomMasterViewModel>("Menuuuuuuu");
            masterDetailNavContainer.Init("Menuuuuuuu");
            masterDetailNavContainer.AddPage<StartViewModel>(StartViewModel.Title);
            masterDetailNavContainer.AddPage<SecondViewModel>(ThirdViewModel.Title);
            masterDetailNavContainer.AddPage<ThirdViewModel>(ThirdViewModel.Title);
            masterDetailNavContainer.AddPage<TestStartViewModel>();
            MainPage = masterDetailNavContainer;
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
