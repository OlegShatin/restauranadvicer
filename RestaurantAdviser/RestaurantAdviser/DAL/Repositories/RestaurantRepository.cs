﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.RequestModels;

namespace RestaurantAdviser.DAL.Repositories
{
    public class RestaurantWebRepository : IRepository<Restaurant>
    {
        private readonly RestClient _restClient;
        private const string GetByIdPathPart = "/api/Restaurants/";
        private const string FilterPathPart = "/api/Restaurants/filter";
        private const string GetAllPart = "/api/Restaurants";
        public RestaurantWebRepository(RestClient client)
        {
            _restClient = client;
        }
        public async Task<IEnumerable<Restaurant>> GetAll()
        {
            var res = await _restClient.GetData<List<Restaurant>>(GetAllPart, HttpMethod.Get);
            return res;
        }

        public  Task<Restaurant> Get(int id)
        {
            return _restClient.GetData<Restaurant>(GetByIdPathPart + id, HttpMethod.Get);
        }

        public async Task<IEnumerable<Restaurant>> Filter(IRequestModel<Restaurant> requestModel)
        {
            var res = await _restClient.GetData<List<Restaurant>>(FilterPathPart, HttpMethod.Post, requestModel);
            return res;
        }
    }
}
