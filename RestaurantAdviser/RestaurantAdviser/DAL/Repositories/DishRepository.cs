﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.RequestModels;

namespace RestaurantAdviser.DAL.Repositories
{
    public class DishWebRepository : IRepository<Dish>
    {
        private readonly RestClient _restClient;
        private const string GetByIdPathPart = "/api/Dishes/";
        private const string FilterPathPart = "/api/Dishes/filter";
        private const string GetAllPart = "/api/Dishes";
        public DishWebRepository(RestClient client)
        {
            _restClient = client;
        }

        public Task<Dish> Get(int id)
        {
            return _restClient.GetData<Dish>(GetByIdPathPart + id, HttpMethod.Get);
        }

        public async Task<IEnumerable<Dish>> Filter(IRequestModel<Dish> requestModel)
        {
            var res = await _restClient.GetData<List<Dish>>(FilterPathPart, HttpMethod.Post, requestModel);
            return res;
        }

        public async Task<IEnumerable<Dish>> GetAll()
        {
            var res = await _restClient.GetData<List<Dish>>(GetAllPart, HttpMethod.Get);
            return res;
        }

        
    }

    
}
