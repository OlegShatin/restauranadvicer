﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestaurantAdviser.Models.RequestModels;

namespace RestaurantAdviser.DAL
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Get(int id);
        Task<IEnumerable<T>> Filter(IRequestModel<T> requestModel);
    }
}
