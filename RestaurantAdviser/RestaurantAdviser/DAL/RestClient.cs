﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestaurantAdviser.Common;
using RestaurantAdviser.Models.DbEntities;

namespace RestaurantAdviser.DAL
{
    public class RestClient
    {
        private readonly ILogger _logger;
        private const string Domain = "http://restseu.azurewebsites.net";
        private HttpClient _httpClient = new HttpClient();

        public RestClient(ILogger logger)
        {
            _logger = logger;

            //_httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            //_httpClient.DefaultRequestHeaders.Add("Content-Type", "application/json");
        }

        public async Task<TExpected> GetData<TExpected>(string pathPart, HttpMethod method, object data = null)
        {
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(Domain + pathPart),
                Method = method
            };
            request.Headers.Accept.ParseAdd("application/json; utf-8");
            if (method == HttpMethod.Post && data != null)
                request.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            try
            {
                using (HttpResponseMessage response = await _httpClient.SendAsync(request))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string json = await response.Content.ReadAsStringAsync();
                        TExpected result = JsonConvert.DeserializeObject<TExpected>(json);
                        return result;
                    }
                    else
                    {
                        _logger.Log(
                            $"Resp code: {response.StatusCode} when getting data. path: {pathPart}, data: {data}, mess: {response.Content}");
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                _logger.Log($"TCE when getting data. path: {pathPart}, data: {data}, exc: {tce}");
            }
            catch (HttpRequestException hre)
            {
                _logger.Log($"HRE when getting data. path: {pathPart}, data: {data}, exc: {hre}");
            }
            catch (Exception e)
            {
                _logger.Log($"Exception when getting data. path: {pathPart}, data: {data}, exc: {e}");
            }
            return default(TExpected);
        }
    }
}