﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using FakeItEasy;
using NUnit.Framework;
using NUnit.Framework.Internal;
using RestaurantAdviser.DAL;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.RequestModels;
using ILogger = RestaurantAdviser.Common.ILogger;

namespace RestaurantAdvicerTests.DAL
{
    [TestFixture]
    public class RestClientShould
    {
        private RestClient _client;
        [SetUp]
        public void SetUp()
        {
            ILogger logger =  A.Fake<ILogger>();
            A.CallTo(() => logger.Log(A<string>._)).Invokes((string arg) => Console.WriteLine(arg));
            _client = new RestClient(logger);
        }

        [Test]
        public void ReturnsData_WhenSingleObjectRequestCorrect()
        {
            var res = _client.GetData<Restaurant>("/api/Restaurants/" + 1, HttpMethod.Get).Result;
            Is.True.ApplyTo(res.Id == 1);
        }
        [Test]
        public void ReturnsFiltredData_WhenFilterRequestCorrect()
        {
            var res = _client.GetData<IEnumerable<Dish>>("/api/Dishes/filter", HttpMethod.Post, new DishRequest(){RequiredType = DishesType.Eat}).Result;
            Is.True.ApplyTo(res.All(d => d.Type == DishesType.Eat));
        }
    }
}
