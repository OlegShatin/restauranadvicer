﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FakeItEasy;
using NUnit.Framework.Internal;
using NUnit.Framework;
using RestaurantAdviser.DAL;
using RestaurantAdviser.Models.DbEntities;
using RestaurantAdviser.Models.Generated;
using RestaurantAdviser.Services.Impls;

namespace RestaurantAdvicerTests.Services
{
    [TestFixture]
    public class UserTestServiceImplShould
    {
        private UserTestServiceImpl _service;

        private List<Dish> _dishes = new List<Dish>()
        {
            new Dish() {Name = "Суп", Description = "Грибной", Id = 1},
            new Dish() {Name = "Котлета", Description = "по киевски", Id = 2}
        };
        private List<Restaurant> _rests = new List<Restaurant>()
        {
            new Restaurant() {Name = "RestWithSoup", Id = 1, RestaurantDishes = new List<RestaurantDish>() {new RestaurantDish() {DishId = 1, RestaurantId = 1}}},
            new Restaurant() {Name = "RestWitMeat", Id = 2, RestaurantDishes = new List<RestaurantDish>() {new RestaurantDish() {DishId = 2, RestaurantId = 2}}}
        };
        [SetUp]
        public void SetUp()
        {
            var drepo = A.Fake<IRepository<Dish>>();
            A.CallTo(() => drepo.Filter(null)).WithAnyArguments().Returns(_dishes);
            A.CallTo(() => drepo.GetAll()).Returns(_dishes);
            var rrepo = A.Fake<IRepository<Restaurant>>();
            A.CallTo(() => rrepo.Filter(null)).WithAnyArguments().Returns(_rests);
            A.CallTo(() => rrepo.GetAll()).Returns(_rests);
            _service = new UserTestServiceImpl(drepo, rrepo);
        }

        [Test]
        public void OfferOnlyVeganDish_WhenThisSelected()
        {
            var testRes = new UserTestResult()
            {
                EatType = EatType.HomeDish, WithoutMeat = true
                
            };
            var res = _service.OfferOrdersByTest(testRes).Result;

            Is.True.ApplyTo(res.All(o => o.Dishes.All(d => d.Id != 2)));
        }
    }
}