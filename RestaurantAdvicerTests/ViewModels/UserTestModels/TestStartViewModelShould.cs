﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeItEasy;
using FreshMvvm;
using FreshMvvm.Base;
using NUnit.Framework;
using RestaurantAdviser.ViewModels;
using RestaurantAdviser.ViewModels.UserTestModels;

namespace RestaurantAdvicerTests.ViewModels.UserTestModels
{
    
    [TestFixture]
    public class TestStartViewModelShould
    {
        private TestStartViewModel _model;

        [SetUp]
        public void SetUp()
        {
            
            _model = new TestStartViewModel();
            
        }

        [Test]
        public void OpenNextPage_WhenStartPageCommandExecuted()
        {
            var fakedNav = A.Fake<IPageModelNavigation>();
            _model.Navigation = fakedNav;

            _model.StartTestCommand.Execute(null);

            A.CallTo(() => fakedNav.PushPageModel<SimpleItemViewModel>(true)).WithAnyArguments().MustHaveHappened();
        }
    }
}
