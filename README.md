# RestaurantAdvicer #

This README would normally document whatever steps are necessary to get your application up and running.

## Used frameworks ##

* [Fody](https://github.com/Fody/PropertyChanged) - AOP framework to avoid iplementing INotifyPropertyChanged 
* [FreshMvvm](https://github.com/rid00z/FreshMvvm) - MVVM framework to bind Page to ViewModel
    * UPD: for migration onto .NETStandard 1.4 instead of Nuget package of FreshMvvm now is used [Unofficial Fork with .NETStandard](https://github.com/Mark-RSK/FreshMvvm) as another project in solution. There are some undocumented diffs.



## Conventions and rules##
* All starting configurations: see App.xaml.cs constructor
    * Navigation stack is special type of page, which contains other pages. Its common  to checking out in only one nav stack, but if you need, you can create another to switch type (simple | master-detail | etc.).
    * To manage navigation in ViewModel use `Navigation` property, insead of `CoreMethods`
        * To add page to our custom Master-Detail use `Navigation.AddPageToNavigation<ToAddViewModelType>()`
        
                public Command AddCommand
                {
                    get
                    {
                        if (_addCommand == null)
                            _addCommand = new Command(() =>
                            {
                                Navigation.AddPageToNavigation<SimpleItemViewModel>();
                            });
                        return _addCommand;
                    }
                }
                
    * All instances should be created via IoC-container. For injecting create single public constructor for class and add params to it. Registry your services in `App.xaml.cs` constructor and they will be injected.
	
* __Naming convention rules__ - used for resolve ViewModel<->View
    * All ViewModels classes should have `ViewModel` postfix. Example: `SampleViewModel`
    * All full pages, which does not included by another elements should have `View` postfix. Example: `SampleView`
    * All elements, which can be included by another elements should have `ElemView` postfix. Example: `SampleElemView`    
	
* All ViewModels must be stored in `[ProjectName].ViewModels` namespace and extend `FreshPageModel` class.

* All XAML views should be stored in `[ProjectName].Views` namespace. 
    * `ElemView`'s should extend `ContentView`
    * `View`'s should extend `ContentPage`
	
* Custom Master-Detail has next rules.
    * When You use `Navigation.AddPageToNavigation<SimpleItemViewModel>();`, You should ensure You have two Views: for menu elem and page for detail. *todo: make default when not found?*
    * By resolving created three objects: `ViewModel`, `ElemView` = `ContentView`, and `View` = `ContentPage`. `ViewModel` will be shared as the same `BindingContext` for `ElemView` and `View`
	
* You should remove `[XamlCompilation (XamlCompilationOptions.Compile)]` attributes in views, it'll not working.
* Please see FreshMVVM docs to clear magic about ViewModels-Pages-IoC. 
* See last version for samples
* To create view model use next template:
        Template:
        
            //attribute for Fody to auto impl INPChanged interface 
            [AddINotifyPropertyChangedInterface]
            class SomeViewModel : FreshPageModel
            {
                //prop for using in static contexts
                public static string Title { get; } = "SomePage";
                //prop for using in binding
                public string TitleVal => Title;
                
                public string SomeProperty { get; set; } = "Alex";
                
                //use DInjection
                private IService _service;
                public SomeViewModel(IService service)
                {
                    _service = service;
                }
                
                private Command _someCommand;
                //do not use new command for each call - save command into private field
                public Command SomeCommand
                {
                    get
                    {
                        if (_someCommand == null)
                        //use async/await to not block ui
                            _someCommand = new Command(async () =>
                            {
                                //do command stuff
                                await service.LongOperationToNotBlockingUi();
                            });
                        return _someCommand;
                    }
                }
            }
			

##Customizing##
* If there is no elem view class for some ViewModel, then will be found class with name `DefaultElemView` and setted as ElemView for this ViewModel
* You can share specified elem views (and pages, but why?) between several ViewModels using mapping rules:
	
	    FreshPageModelMapper.SetMappingRuleForElem<FirstViewModel, SharedElemView>();
	    FreshPageModelMapper.SetMappingRuleForElem<SecondViewModel, SharedElemView>();

See `App.xaml.cs` for sample.
 
* Priority for resolving Views for ViewModels:
    1. Custom rules using `FreshPageModelMapper.SetMappingRuleForElem`
	2. Naming convention
	3. Default (for elems only)

* Customizing __MasterDetailPageMaster__ Part. You can specify Your own Master layout by following the next steps
    * Add view to `Views` package with the following class signature:
	
	      public partial class YourMasterView : ContentPage, ICustomMaster

      Do not forget implement ICustomMaster interface, which provided property for some `StackLayout` in Your layout. Into this stack views will be added dynamically.
      
    * Create ViewModel for this View in `ViewModels` f.e. `YourMasterViewModel : FreshPageModel`. This class will 	work like simple binded viewmodel for YourMasterView (and only for it, not for dynamically elements in stack)
    
	* Init `CustomMasterDetailNavContainer` with this types using the following code in App.xaml.cs
	
            var masterDetailNavContainer = new CustomMasterDetailNavigationContainer(Data.Consts.NavigationContainerNames.MainContainer);
            masterDetailNavContainer.Init<YourMasterView, YourMasterViewModel>("Menuuuuuuu");

	* If you dont need navbar into your page, call `NavigationPage.SetHasNavigationBar(this, false);` into your page constructor (code-behind class).